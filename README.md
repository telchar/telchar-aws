Biblioteca que disponibiliza facilidades para conexão com as ferramentas da AWS.

## Utilização

    const aws = require('telchar-aws');


## Configuração

Para utilizar essa lib precisa configurar as seguintes variáveis de ambiente:

**AWS_KMS_KEY_ID**
_Obrigatório (em lambda ja vem preenchido)_
Id da chave KMS que será utilizada

**AWS_ACCESS_KEY_ID**
_Obrigatório (em lambda ja vem preenchido)_
Id do Access Key para acessar a conta da amazon a qual a chave KMS pertence

**AWS_SECRET_ACCESS_KEY**
_Obrigatório (em lambda ja vem preenchido)_ 
Secret Key vinculado com a Access Key informada

**AWS_REGION**
_Obrigatório_
Região em que se encontra a chave KMS

## KMS

Esta biblioteca KMS trabalha com apenas a chave configurada na variavel de ambiente _KMS_KEY_ID_

**encrypt**
Encripta um texto simples, retornando uma Promise.

    const aws = require('telchar-aws');
    const value = aws.kms.encrypt('a plain text');

**encryptJSON**
Variação do _encrypt_, serializando um objeto JSON

    const aws = require('telchar-aws');
    const value = aws.kms.encrypt({message: 'a json object'});

**decrypt**
Decripta um texto criptografado.

    const aws = require('telchar-aws');
    const value = aws.kms.decrypt(encryptedText);


**decryptJSON**
Variação do _decrypt_, deserializando de um objeto JSON

    const aws = require('telchar-aws');
    const json = aws.kms.decryptJSON(encryptedText);
