require('dotenv').config();
const { kms } = require('../src/index');

kms.encryptJSON({
  name: 'Oswald Chesterfield Cobblepot',
  nick: 'Penguin',
  type: 'supervillain',
}).then(console.log);

kms.decryptJSON('AQICAHgUgPMdVEkRMvu/Wp2v3MtW8SqcWVatv+/7z6/TSYiAMgGxPWZYg6B+NKke5F17nZr/AAAAsTCBrgYJKoZIhvcNAQcGoIGgMIGdAgEAMIGXBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDA5Uj7+A1YLL64syCgIBEIBqPdCYL/yYaRPVgCfXowKQ7OIkflowGJ+4aOrqzlCmikpybaWlrgTV78wxoPbcQfw7pV7UFW/51PPF78IQXCTB1wv1sHUsEwkrdhu1ommKd0P2NVDBtccyAdDHjZRbB8q45/Ygob4wmTIExw==')
  .then(console.log);
