const aws = require('aws-sdk');
const config = require('./config');
const KeyId = config.KMS_KEY_ID;

const kms = new aws.KMS({});

const encrypt = Plaintext => new Promise((resolve, reject) => {
  kms.encrypt({
    KeyId,
    Plaintext,
  }, (err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data.CiphertextBlob.toString('base64'));
    }
  });
});

const decrypt = cipher => new Promise((resolve, reject) => {
  kms.decrypt({
    CiphertextBlob: Buffer.from(cipher, 'base64'),
  }, (err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data.Plaintext.toString('utf-8'));
    }
  });
});

module.exports = {
  encrypt,
  encryptJSON: json => encrypt(JSON.stringify(json)),
  decrypt,
  decryptJSON: json => decrypt(json).then(r => JSON.parse(r)),
};
