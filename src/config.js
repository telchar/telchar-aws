require('require-environment-variables')([
  'AWS_KMS_KEY_ID',
]);

module.exports = {
  KMS_KEY_ID: process.env.AWS_KMS_KEY_ID,
};
