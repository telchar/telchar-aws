jest.mock('../config', () => ({ KMS_KEY_ID: 'FAKE_KMS_KEY' }));
jest.mock('aws-sdk', () => {
  const kms = {
    encrypt: jest.fn(),
    decrypt: jest.fn(),
  };

  const KMS = jest.fn().mockImplementation(() => kms);

  return { kms, KMS };
});
const aws = require('aws-sdk');
const kms = require('../kms');

describe('kms encription', () => {
  beforeEach(() => {
    aws.kms.encrypt.mockClear();
    aws.kms.decrypt.mockClear();
  });

  it('should configure', () => {
    expect(aws.KMS).toHaveBeenCalledWith({});
  });

  it('should encrypt a text', async () => {
    const expectedResult = 'ZXhwZWN0ZWQgdGV4dA==';
    aws.kms.encrypt.mockImplementation((data, cb) => cb(null, { CiphertextBlob: Buffer.from(expectedResult, 'base64') }));
    const result = await kms.encrypt('opa');
    expect(result).toBe(expectedResult);
  });

  it('should handle encrypt error', async () => {
    const expectedError = 'expected error';
    try {
      aws.kms.encrypt.mockImplementation((data, cb) => cb(new Error(expectedError)));
      await kms.encrypt('opa');
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e.message).toBe(expectedError);
    }
  });

  it('should encrypt a JSON', async () => {
    const expectedResult = 'ZXhwZWN0ZWQgdGV4dA==';
    aws.kms.encrypt.mockImplementation((data, cb) => cb(null, { CiphertextBlob: Buffer.from(expectedResult, 'base64') }));
    const result = await kms.encryptJSON({ teste: 'opa' });
    expect(result).toBe(expectedResult);
  });

  it('should decrypt a text', async () => {
    const expectedResult = 'expectedRESULT';
    aws.kms.decrypt.mockImplementation((data, cb) => cb(null, { Plaintext: Buffer.from(expectedResult, 'utf-8') }));
    const result = await kms.decrypt('ZXhwZWN0ZWQgdGV4dA==');
    expect(result).toBe(expectedResult);
  });

  it('should handle decrypt error', async () => {
    const expectedError = 'expected error';
    try {
      aws.kms.decrypt.mockImplementation((data, cb) => cb(new Error(expectedError)));
      await kms.decrypt('opa');
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e.message).toBe(expectedError);
    }
  });

  it('should decrypt a JSON', async () => {
    const expectedResult = '{"teste": "opa"}';
    aws.kms.decrypt.mockImplementation((data, cb) => cb(null, { Plaintext: expectedResult }));
    const result = await kms.decryptJSON('ZXhwZWN0ZWQgdGV4dA==');
    expect(result).toEqual({
      teste: 'opa',
    });
  });
});
